---
title: Petition Information
type: docs
---
![Header Image](https://stopcuts.github.io/images/ensembles_header.png "header")
# Call to Alumni, Graduate Students, Community Stakeholders, Etc.

**We have learned that McMaster University’s School of the Arts intends to cease offering the David Gerry Flute Ensemble (MUSIC 1-4GF3) and the McMaster Jazz Band (MUSIC 1-4GJ3) annually.** As alumni of McMaster’s Bachelor of Music program, graduate students in McMaster’s Institute for Music and the Mind, and active musicians in the Hamilton arts community, we believe this decision will negatively affect the McMaster community and should be reconsidered. **Failing to support these ensembles would limit both curricular and extra-curricular opportunities for students and weaken the institution's reputation for fostering the arts and connections between disciplines.** We therefore ask for your support in raising awareness about the ongoing cuts to opportunities in music at McMaster and their potential consequences.

<--->

## Ensembles At McMaster

Musical ensembles have thrived at McMaster since the 1960s. McMaster’s ensembles attract a variety of students from diverse areas of studies including Engineering, Health Science, Science, Commerce, Arts & Sciences, and Humanities. Although many of these students participate in ensembles as purely extra-curricular activities, some students enroll for elective course credit. Low enrollment rates are innate to the structure of some ensembles: for instance, a “big band” style ensemble like the McMaster Jazz Band cannot support more than 20 students (typically five of each instrument group, i.e., saxophones, trombones, trumpets, and rhythm section). Furthermore, students from faculties such as Engineering can experience technical barriers to officially enrolling in ensemble credits. **Although these limitations to enrollment are often practical, the School of the Arts is reducing the jazz and flute ensembles on the basis of enrollment alone.**

<--->

## Why Does Group Music Making Matter?

Of course, ensembles are not only elective courses. Students completing a Bachelor of Music are required to complete 4 ensemble credits, one in each year of their studies.**The discontinuity of offering certain ensembles biennially would affect students’ abilities to complete the necessary requirements for their degrees, especially students who specialize in certain instruments and styles, who would be forced to spend two years – half of their degrees – playing in ensembles outside of their discipline.** Beyond the obvious detriment to the students’ education, this decision can also reduce the passion and expert characteristic of McMaster’s ensembles.

<--->

In essence, although ensembles at McMaster are offered as courses, **we believe that treating ensembles like traditional lecture-based courses that can be offered now and then or substituted for one another is unwise** and ultimately harmful to McMaster’s music community.

<--->

Finally, the benefits of offering ensembles go beyond the curricular. McMaster prides itself on being an institution which fosters interdisciplinary education. Both of us completed a Bachelor of Music (specialization in Music Cognition) at McMaster which allowed us to pursue graduate studies in the Faculty of Science (department of Psychology, Neuroscience, & Behaviour). However, **interdisciplinarity is a two-way street.** It is not enough to create opportunities for one discipline to study another, both must thrive to have a truly interdisciplinary approach. Beyond the academic, providing robust musical experiences for students from all backgrounds has immense benefits for mental and physical health, social cohesion, and quality of life ***([see the following resources](https://linktr.ee/mcmasterensembles)).*** 

## Call To Action

We are calling on anyone who is concerned about the School of the Art’s decisions regarding this cut to essential musical ensembles and community music making to contact McMaster University administration and urge them to reconsider. **We encourage you to email the following administrators at McMaster to express your concerns regarding these decisions; as well as any personal connections to music at McMaster and the arts; and how you believe these decisions will affect the accessibility to music education at McMaster and Ontario at large.**


| Director, School of the Arts: 	| Provost & Vice-President (Academic): 	| Dean of Humanities: 	| President and Vice-Chancellor: 	|
|:-----------------------------:	|:------------------------------------:	|:-------------------:	|:------------------------------:	|
|    Dr. Stephanie Springgay    	|            Dr. Susan Tighe           	|   Dr. Pamela Swett  	|        Dr. David Farrar        	|
|      sotadir@mcmaster.ca      	|          provost@mcmaster.ca         	|  swettp@mcmaster.ca 	|      president@mcmaster.ca     	|


If you are comfortable, please Cc/Bcc. Cam Anderon andersoc@mcmaster.ca and Konrad Swierczek swierckj@mcmaster.ca so that we can keep track of the support and number of emails being sent.

In addition, please fill out our ***[Google Form Petition](https://tinyurl.com/macjazzandflute).*** 


Feel free to forward this email to anyone who may be willing to support.
See below of variety of graphics for sharing on social media or elsewhere.

In Solidarity,

**Konrad Swierczek**;
B.Mus. (specialization, Music Cognition), McMaster University
Ph.D. Student; Psychology, Neuroscience, & Behaviour, McMaster University.

&

**Cam Anderson**;
B. Mus. (specialization, Music Cognition), McMaster University.
Ph.D. Student; Psychology, Neuroscience, & Behaviour, McMaster University. 
