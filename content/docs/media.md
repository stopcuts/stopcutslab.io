---
title: Social Media Images
type: docs
---
![Header Image](https://stopcuts.github.io/images/ensembles_header.png "header")

# Social Media Images

Circulate these images in social media posts to raise awareness about cuts to music opportunities at McMaster.

## Instagram Story Image

![Long Poster](https://stopcuts.github.io/images/ensembles_story.png "story")

## Square Poster

![Poster Image](https://stopcuts.github.io/images/ensembles_square.png "poster")

## Wide Poster

![Wide Poster](https://stopcuts.github.io/images/ensembles_wide.png "wide_poster")

## Profile Picture

![Header Image](https://stopcuts.github.io/images/ensembles_profile.png "profile")

## Banner Image

![Banner Image](https://stopcuts.github.io/images/ensembles_header.png "banner")
