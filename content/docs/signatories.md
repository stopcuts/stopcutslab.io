![Header Image](https://stopcuts.github.io/images/ensembles_header.png "header")
 ## 190 Signatures and Counting.
|Name                        |Affiliation                                                                                 |
|:---------------------------|:-------------------------------------------------------------------------------------------|
|Konrad Swierczek            |McMaster University (PhD Student)                                                           |
|Cameron Anderson            |McMaster University (Graduate Student in Psychology, Neuroscience & Behaviour)              |
|Dylan Matsuda               |McMaster University                                                                         |
|Oishee Ghosh                |McMaster University, Arts & Science program, Level III                                      |
|Elan Yaphe                  |Student                                                                                     |
|Max Delle Grazie            |McMaster University (Student)                                                               |
|Severine Stephanie Salvador |Musician/Music Instructor                                                                   |
|Julia Bissessar             |SOTA                                                                                        |
|Andres Elizondo Lopez       |Graduate Student                                                                            |
|Joanna Spyra                |McMaster, PhD candidate                                                                     |
|Jamaar Bryan                |McMaster alumni                                                                             |
|Daryl Buckle                |Self-employed (Lesson Instructor/Freelance Musician)                                        |
|Benjamin Kelly              |McMaster BSc Student                                                                        |
|Graham Young                |McMaster grad 1974                                                                          |
|Alexander Nguyen            |ProSensus, Project Engineer                                                                 |
|Alyssa Atkinson             |Sales & Rental Associate/Long & McQuade                                                     |
|Jennifer Ferreira           |University of Lethbridge, Bachelor of Music                                                 |
|Michael Ku                  |Medical Student at University of Ottawa Faculty of Medicine                                 |
|Claire Larson               |Graduate Student                                                                            |
|Stephanie An                |Mcmaster University - Student                                                               |
|Madeline Deane              |McMaster University, Science Student                                                        |
|Adam Katz                   |McMaster                                                                                    |
|Ellis Ritz                  |Former student                                                                              |
|Joel Affoon                 |Mcmaster music student                                                                      |
|Janelle Burton              |McMaster Music Alumni                                                                       |
|Ailish Corbett              |Teacher                                                                                     |
|Sean Parkinson              |Local Artist                                                                                |
|Ben Allan                   |Musician                                                                                    |
|Keegan Larose               |Music Instructor                                                                            |
|Matt Vukovic                |McMaster                                                                                    |
|Christina Doan              |McMaster University Grad Student                                                            |
|Daria Shkredova             |Radboudumc PhD Candidate                                                                    |
|Katherine D'agostino        |Undergraduate Student                                                                       |
|Riley Genier                |McMaster University Alumni                                                                  |
|Maggi Djurdjevic            |Student                                                                                     |
|Rabekah Heintzman           |Mcmaster music- 5th year student                                                            |
|Isabel Diavolitsis          |McMaster University                                                                         |
|Daniel Antonenko            |McMaster alum                                                                               |
|Vanessa Mountaiin           |Music Student                                                                               |
|Haleigh Wallace             |McMaster University student, Undergraduate Representative on the McMaster University Senate |
|Louise Concepcion           |McMaster University - Student                                                               |
|Victoria Bozzo              |Western University - Bachelor of Education                                                  |
|Emily Wood                  |PhD Student, McMaster Institute of Music and the Mind                                       |
|Julia Paddock               |Arts Teacher                                                                                |
|Emily Lightstone            |GHD, Water Resources Engineer                                                               |
|Gary Liu                    |McMaster / Student                                                                          |
|Evan Borrelli               |McMaster Student, iBioMed, first year                                                       |
|Benjamin Kersey             |University of Toronto, third-year music student                                             |
|Alyson King                 |McMaster Alumna                                                                             |
|Olivia Presti               |Student                                                                                     |
|Anonymous                   |Artistic Director                                                                           |
|Kristin Weiman              |Teacher                                                                                     |
|Dylan Duarte                |Alumni                                                                                      |
|Sophia Khani                |McMaster University Graduate Student                                                        |
|Thomas Gulyas               |Student                                                                                     |
|Amy                         |Teacher                                                                                     |
|Joshua Guinness             |Student                                                                                     |
|Kelsea Mccready             |School of Advanced Study, University of London/Graduate Student (McMaster Alumnus)          |
|Ben Xamin                   |Student                                                                                     |
|Aylish Hennen               |Student                                                                                     |
|Claudia Spadafora           |n/a                                                                                         |
|Jessica Ostrega             |McMaster University                                                                         |
|Wendy Tang                  |McMaster student                                                                            |
|Jasmine Dyer                |HWDSB teacher                                                                               |
|Meg Noble                   |McMaster SOTA student                                                                       |
|Abdullh Mohamex             |Student at Mcmaster                                                                         |
|Scott Montani               |Drum Corp International Canada                                                              |
|Andrea Brzezinski           |Music Therapist                                                                             |
|Anonymous                   |McMaster/student                                                                            |
|Sarah Lam                   |Music educator                                                                              |
|Jo-Anne Devries             |Grandmother                                                                                 |
|Morgan Teachey              |The Salvation Army Divisional Worship Arts & Education Director                             |
|Claire Patterson            |Student, IBEHS first year McMaster                                                          |
|Naomi Wong                  |University of Toronto, MSc Candidate; McMaster University Alumna                            |
|Elias Elaneh                |Mcmaster (student)                                                                          |
|Maya Flannery               |McMaster PNB (MSc student)                                                                  |
|Robin Coloma                |McMaster Student                                                                            |
|Natasha Wandel              |McMaster University                                                                         |
|Anonymous                   |Fanshawe College                                                                            |
|Anonymous                   |Student                                                                                     |
|Erin Rich                   |Western Grad                                                                                |
|John Lam                    |Head of Performing Arts/Glendale High School                                                |
|Anonymous                   |Royal Military College of Canada - Student                                                  |
|Tabitha Palmer              |Student, University of Dundee                                                               |
|Mary Rigg                   |retired                                                                                     |
|Anonymous                   |Artist                                                                                      |
|Laurie Larson               |NA                                                                                          |
|Jennifer Lin                |McMaster Music Alum                                                                         |
|Michael Larson              |NA                                                                                          |
|Grace Larson                |Student                                                                                     |
|Anonymous                   |Elementary teacher hwcdsb                                                                   |
|Linda                       |None                                                                                        |
|Laura Mcnabb                |McMaster alumna (BMus (cognition))                                                          |
|Mike T                      |Music teacher HDSB                                                                          |
|Casey Mason                 |SCCDSB Secondary Teacher                                                                    |
|Anonymous                   |McMaster                                                                                    |
|Anonymous                   |Teacher                                                                                     |
|Leanne Cherian              |McMaster University/Student                                                                 |
|Anonymous                   |Mcmaster undergrad student                                                                  |
|Jake                        |Humber Music student                                                                        |
|Fran Segado                 |IRCC/ Officer                                                                               |
|Ron Hustins                 |Pringle Creek P.S. (teacher)                                                                |
|Anonymous                   |Vocal teacher at Long and McQuade                                                           |
|Anonymous                   |Grad student, mcmaster                                                                      |
|Adele Lee                   |McMaster Univeresity, Student                                                               |
|Wendy Yu                    |University of Ottawa (MD Year 1)                                                            |
|David Parkes                |Student                                                                                     |
|Kristen Whittle             |student, Wilfrid Laurier University                                                         |
|Jessica Morency             |McMaster Student                                                                            |
|Simone Ocvirk               |McMaster Student                                                                            |
|Jasmine                     |Student                                                                                     |
|Alex Damjanovski            |McMaster Engineering Society (President)                                                    |
|Sarah Cushnie               |McMaster iBioMed student                                                                    |
|Travis Czebe                |McMaster (student)                                                                          |
|Sarah Elgersma              |Graduate of Music Program at McMaster                                                       |
|Taylor Lavigne              |Mcmaster                                                                                    |
|Christine Nguyen            |McMaster University                                                                         |
|Anonymous                   |Student                                                                                     |
|Anonymous                   |McMaster University                                                                         |
|Shoshana Furman             |McMaster student                                                                            |
|Anonymous                   |Student                                                                                     |
|Jessica Lamantia            |McMaster (student)                                                                          |
|Christina Cantlon           |Brock University                                                                            |
|Anonymous                   |McMaster University Alumni                                                                  |
|Olivia Korosak              |Alumni of McMaster's Music Program                                                          |
|Klarissa                    |Student                                                                                     |
|Aleef Mehdi                 |McMaster Graduate                                                                           |
|Adeola Egbeyemi             |McMaster / Undergrad                                                                        |
|Linda Zhuo                  |Western University/OT Student                                                               |
|Amelia Lind                 |McMaster University (undergraduate student)                                                 |
|Heather Wildeman            |Wilfred Laurier University, education student                                               |
|Erin Puersten               |McMaster Alumni                                                                             |
|Michelle Nicol              |McMaster Undergraduate                                                                      |
|Thea Richinson              |University of Guelph (undegraduate)                                                         |
|Ian Brown                   |Mohawk College music graduate, Professional Music Composer, Mix Engineer, Teacher           |
|Anonymous                   |McMaster student                                                                            |
|Anonymous                   |McMaster Student                                                                            |
|Karina Király               |Mcmaster student - Social Science I                                                         |
|Julia Barnes                |Student at McMaster University                                                              |
|Chelsea Mackinnon           |Sessional Instructor/Alumni, McMaster University                                            |
|Olivia Mcisaac              |McMaster undergraduate                                                                      |
|Anonymous                   |Student                                                                                     |
|Dahlia Taylor               |Student                                                                                     |
|Gabrielle Katz              |2nd Year Health Sciences Student at McMaster University                                     |
|Steve Brown                 |TDSB music teacher                                                                          |
|Phil Rayment                |Teacher - HWDSB                                                                             |
|Toni-Rose Asuncion          |n/a                                                                                         |
|Taylor M                    |McMaster                                                                                    |
|Anonymous                   |McMaster                                                                                    |
|Riley Kay                   |Student                                                                                     |
|Andrew Canete               |McMaster University Student                                                                 |
|Elizabeth Phillips          |PhD Candidate, McMaster, PNB                                                                |
|Brett Scully                |McMaster University                                                                         |
|Cordell Lewis               |Mac music student                                                                           |
|Rachel Reid                 |Student/McMaster University                                                                 |
|Anita Richard               |Instructional Designer/eLearning Developer                                                  |
|Cathie Koehnen              |High school music teacher                                                                   |
|Daryl Yaeger                |Skyliners Youth Big Band - Musical Director                                                 |
|Sabra Salim                 |McMaster Graduate Student (MSC)                                                             |
|Anonymous                   |Student                                                                                     |
|Mathew Montani              |Hamilton Academy of Music (Co-Owner, Head piano teacher)                                    |
|Graeme Noble                |Lab Manager/Research Associate                                                              |
|Eric Burton                 |McMaster University alumni                                                                  |
|Miranda                     |Western University B.Ed Alumni                                                              |
|Jung Pao                    |McMaster Symphony Orchestra/Harp                                                            |
|Donya Sandhu                |medical editor                                                                              |
|Grace Hugill                |McMaster Student                                                                            |
|Eoin Anderson               |Commercialization manager                                                                   |
|Michael Coate               |OCT (Ontario Certified Teacher)                                                             |
|Anne Claire Baguio          |McMaster Alumni                                                                             |
|Christian Filice            |Student                                                                                     |
|Anonymous                   |Student                                                                                     |
|Connie Couture              |STM Music Teacher                                                                           |
|Laura Street                |McMaster (2021), UofT (Student)                                                             |
|Carmelina Palacios          |Former McMaster/Student                                                                     |
|Anonymous                   |Brock University                                                                            |
|Lucas K.                    |McMaster University, graduate student                                                       |
|Erica Flaten                |McMaster Unicersity/PhD Candidate                                                           |
|Anonymous                   |Student                                                                                     |
|Joey Tang                   |Secondary School Teacher, Toronto District School Board                                     |
|Denver Della-Vedova         |McMaster alumni                                                                             |
|Gerry Anderson              |Office Assistant, SCDSB                                                                     |
|Keiko Gutierrez             |McMaster University (Grad Student/TA)                                                       |
|Twila Vanderkooy            |Music Teacher/ Dundas Conservatory of Music                                                 |
|Hannah Macdougall           |Masters of Public Administration Candidate - Dalhousie University                           |
|Rachael Finnerty            |Sessional Instructor, PhD Student                                                           |
|Phillip Darley              |McMaster University (student)                                                               |
|Anonymous                   |Parent                                                                                      |
|Anushya Elmer               |McMaster alumnus and parent                                                                 |
|Anonymous                   |Student                                                                                     |
